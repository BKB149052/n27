# 15.02.2019


## Logout

index.ejs:
```
<a href="/login" class="button">Abmelden</a>
```

server.js:
```
app.get('/login',(req, res, next) => {          
    // 22.02.
    res.cookie('istAngemeldetAls', '')
    res.render('login.ejs', {
    })    
})
```

## Der User wird *persönlich* begrüßt.

index.ejs
```
<h2>Willkommen <%= kundenId %></h2>
```

server.js
```
app.post('/',(req, res, next) => {     
    
  ...
        res.render('index.ejs', {
            kundenId: kundenId
        })
    ...
})
```

